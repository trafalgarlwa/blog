---
title: 开发知识技能整理
date: 2022-03-18 00:44
---

<!-- toc -->

# 软件开发

---

## WEB 服务端编程

---

### Java EE 企业级应用

- Tomcat，Servlet，JSP 传统 web 编程

### Spring 高级编程

- Spring Framework（Spring IOC 容器为其核心）
- Spring Boot（其设计目的是用来简化新 Spring 应用的初始搭建以及开发过程。该框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置）
- Web 框架
  - Spring MVC
- ORM 框架
  - Spring Data JPA
  - Spring Mybatis （Or Mybatis Plus）
- 权限与安全管理
  - Spring Security
  - Spring Shiro
- 缓存服务
  - Redis
- 消息队列服务
  - Rabbit MQ （Spring 出品）
  - Rocket MQ
  - Kafka（高吞吐量的分布式发布订阅消息系统）

### Play for Java  or Play for Scala

高性能 WEB 框架

### Vert.x 编程框架

响应式，高性能异步编程框架，非 Web 容器，非 Web Server

### Golang Web 编程

- [awesome-go](https://github.com/avelino/awesome-go) 一系列关于 Go 框架，库和软件的列表

- 路由框架
  - Httprouter
- Web 框架
  - Gin
  - Iris（全方面的 web 框架，支持路由，ORM，模型数据绑定等）
  - Beego（基于 MVC 模式的 web 框架）
  - Echo
  - Macaron（不再维护）
  - Gorilla/mux（基础路由框架）
  - Negroni（net/http 处理库）
  - Fasthttp（对应 net/http 的基础服务库）
  - Fiber（底层使用 Fasthttp 库处理）
  - Go-chi（轻量级可组合的路由）
  - Revel

### Node.js Web 编程

- Express.js
- Nest.js（基于 Express.js 底层构建）
- Egg.js（基于 Koa.js 底层构建）
- Koa.js（http 处理库，koa-xx 为相关中间件）
- Sails.js
- Restify.js

### Python Web 编程

- Django
- Flask

### PHP Web 编程

- Think PHP（暂未使用）

### Kotlin Web 编程

- Ktor

### 数据库

- MySQL 5.x，MySQL 8.x（常用开源数据库）
- SQLite（嵌入式数据库）
- SQL Server（微软的数据库产品）
- Postgre SQL
- H2（内存型数据库，使用 Java 编写）
- Oracle

### 用户身份统一认证平台

- [auth0](https://auth0.com) 安全访问平台
- Keycloak 可自建的开源的安全访问平台，支持多种编程语言

---

## WEB 前端编程

---

### Angular.js

Google 开发的 web 前端框架

### Vue.js

单页面应用框架，数据驱动编程，使用 MVVM 模型开发应用

### React.js

单页面应用开发框架

---

## 项目管理

---

### 版本控制系统（VCS）

#### Git

开源的，最常用的分布式版本控制系统

#### SVN

Apache Subversion 的缩写，开源的版本控制系统

### 代码托管平台

#### GitHub

全球最大的代码托管平台，拥有众多的优质开源项目

#### GitLab

自建的代码托管平台

#### Gitee

国产的代码托管平台

#### Gogs

golang 编写，开源的自建代码托管平台，Unknown 大神编写

#### Gitea

golang 编写，开源的自建代码托管平台，由 Gogs 衍生

### 项目管理工具

#### Jira

敏捷开发，一个优秀的缺陷跟踪与项目管理软件

#### Jenkis

持续集成

#### CircleCI

持续集成 github

#### Gerrit

代码评审

#### Openkm

构建仓库

#### Confluence

Atlassian Confluence（简称Confluence）是一个专业的wiki程序。它是一个知识管理的工具，通过它可以实现团队成员之间的协作和知识共享。

### 绘图工具

#### draw.io

可绘制 UML，流程图，项目管理图等

#### StarUML

UML 开发工具

#### ProcessOn

在线绘图网站，可绘制常见的简单图形

#### PlantUML

基于文本的绘图工具，底层通过 Graphviz 实现

#### Graphviz

基于文本的绘图工具

---

## Linux 操作系统

---

### 发行版本

#### Debian

apt 软件包管理

#### Ubuntu

apt 软件包管理，基于 Debian 的发行版本

#### Mint

apt 软件包管理，基于 Debian 的发行版本

#### Centos

yum 软件包管理

#### Fedora

#### Archlinux

pacman 软件包管理，滚动发布的版本

#### Manjaro

pacman 软件包管理，基于 Arch 的 Liux 发行版

#### Deepin

深度操作系统，国产操作系统

### 常用工具包

#### ag，grep，pt

字符串搜索工具

#### zsh

一个更好用的终端，搭配 oh-my-zsh 使用极佳

#### tmux

终端复用器，同类工具有 screen，配置文件可选择 [.tmux](https://github.com/gpakosz/.tmux) 项目

### 文本编辑器

#### Vim/Vi

配置文件可选择 SpaceVim 项目

#### Emacs

配置文件可选择 Spacemacs 项目

#### Nano

#### Gedit

#### VsCode

---

## 桌面级 GUI 编程

---

### Java 平台

- Awt
- Swing
- JavaFx

### 微软平台

- MFC
- WinForm
- C# WPF 应用程序

### QT 应用（跨平台，python 版本为 pyqt）

### GTK 应用（跨平台）

---

## 移动端编程

---

### 微信小程序开发

#### UI 库

- ColorUI

#### 组件

- 有赞 Vant

### Android 应用开发

- Java 开发
- Kotlin 开发特性
  - 协程
