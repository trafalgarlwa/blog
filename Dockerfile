FROM node:alpine as builder

COPY . /app

WORKDIR /app

RUN npm ci && npm run build

FROM nginx

EXPOSE 80

COPY --from=builder /app/public /usr/share/nginx/html
